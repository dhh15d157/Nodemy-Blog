# Nodemy K6 - Project Blog
Đây là sản phẩm đầu tay về mảng lập trình web của Đào Hoàng Hải: BLog đọc báo.
Nội dung là BLog báo theo motip như kenh14.vn nhưng đơn giản hơn. Người truy cập blog có thể đọc các bài viết để cập nhật thông tin cũng như đăng ký tài khoản để tương tác với các thành viên khác và viết các bài viết của chính bản thân. 