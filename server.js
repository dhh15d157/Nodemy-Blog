// dotENV config
require('dotenv').config(); 

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const path = require('path');

const db = require('./src/configs/db');
const routes = require('./src/routes');
const configSession = require('./src/configs/session');

// Express init configs
const app = express();
const hostname = process.env.APP_HOST;
const port = process.env.APP_PORT || 4000;

// Connect to MongoDB
db.connect();
configSession(app);

//config EJS
app.set('views', path.join(__dirname, '/src/views')); 
app.set('view engine', 'ejs'); 
// set file static
app.use('/public', express.static(__dirname + '/src/public'));

// Middlewares configs
app.use(morgan('dev'));
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());

// Routes configs
routes(app);

// Catch 404 error and forward function error handler
app.use((req, res, next) => {
    const err = new Error('Not found!');
    console.log(err);
    err.status = 404;
    next(err);
});

// Error handler function
app.use((err, req, res, next) => {
    console.log(err);
    const error = process.env.NODE_ENV === 'development' ? err : {};
    const statusCode = err.status || 500;

    return res.status(statusCode).json({
        error: {
            message: error.message,
        }, 
    });
});

// Listen server configs
app.listen(port, hostname, () => {
    console.log(`🌏 Server is running on ${hostname} with port ${port}...`);
});
