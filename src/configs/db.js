const mongoose = require('mongoose');

const connect = async () => {
    try {
        const URI = `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
        await mongoose.connect(URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
        });
        console.log('✅ Connect to database MongoDB successfully!');
    } catch (err) {
        console.log(`❌ Connect to database MongoDB failure!\nError: ${err}`);
    }
};

module.exports = { connect };
