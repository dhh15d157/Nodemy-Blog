const nodeMailer = require('nodemailer');


let emailAdmin = process.env.EMAIL_USER;
let emailPassword = process.env.EMAIL_PASSWORD;
let emailHost = process.env.EMAIL_HOST;
let emailPort = process.env.EMAIL_PORT;

const sendEmail = (to, subject, htmlContent) => {
    let transporter = nodeMailer.createTransport({
        host: emailHost,
        port: emailPort,
        secure: false, // config ssl
        auth: {
            user: emailAdmin,
            pass: emailPassword,
        },
    });

    let option = {
        from: emailAdmin,
        to,
        subject,
        html: htmlContent,
    };

    return transporter.sendMail(option);
};

module.exports = sendEmail;
