const session = require('express-session');
const connectMongo = require('connect-mongo');

let mongoStore = connectMongo(session);
let sessionStore = new mongoStore({
    url: `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    autoReconnect: true,
    collection: 'sessions'
});

const configSession = (app) => {
    app.use(
        session({
            key: 'express.sid',
            secret: process.env.SECRET_SESSION,
            store: sessionStore,
            resave: false,
            saveUninitialized: false,
            cookie: {
                maxAge: 1000 * 60 * 60 * 24, // 1 day
            },
        })
    );
};

module.exports = configSession;
