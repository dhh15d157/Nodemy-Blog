const bcrypt = require('bcrypt');
const { validationResult } = require('express-validator');
const authService = require('../services/authService');
const userService = require('../services/userService');
const { viValidationSuccess, viValidationErrors } = require('../lang/vi');

const authController = {};
// sign up user
authController.Register = async (req, res) => {
    const SALT_ROUNDS = parseInt(process.env.SALT_ROUNDS);
    let { email, password } = req.body;
    let validationErrors = validationResult(req);
    let userByEmail = await userService.findByEmail(email);

    if (!validationErrors.isEmpty()) {
        const error = validationErrors.errors;
        return res.render('sign-up', { error } );
    }

    if (userByEmail) {
        if (!userByEmail.local.isActive) {
            const error = viValidationErrors.userActiveError;
            console.log(error);
            return res.render('sign-up', { error } );
        }
        if (userByEmail.deletedAt !== null) {
            const error = viValidationErrors.userDelete;
            return res.render('sign-up', { error } );
        }
        const error = viValidationErrors.emailExist;
        return res.render('sign-up', { error } );
    }

    const SALT = await bcrypt.genSalt(SALT_ROUNDS);
    bcrypt.hash(password, SALT, async (err, hashPassword) => {
        if (!err) {
            try {
                await authService.userRegister(
                    email,
                    hashPassword,
                    req.protocol,
                    req.get('host')
                );
                let data = {};
                data.title = 'CONGRATS!'
                data.content = viValidationSuccess.userCreateSuccess(email.split('@')[0]);
                return res.status(201).render('notification', {data});
            } catch (error) {
                await userService.deleteUserByEmail(email);
                let data = {};
                data.content = viValidationErrors.sendMailError;
                data.title = 'CÓ LỖI RỒI ĐẠI VƯƠNG!'
                return res.status(400).render('notification', {data})
            }
        }
    });
};

authController.getVerifyAccount = async (req, res) => {
    let { token } = req.params;
    let userByToken = await userService.findByToken(token);

    if (!userByToken) {
        let data = {};
        data.title = 'CONGRATS!'
        data.content = viValidationErrors.tokenUndefined;
        return res.status(201).render('notification', {data});
    }

    await authService.getVerifyAcount(token);
    let data = {};
    data.title = 'CONGRATS!'
    data.content = viValidationSuccess.accountActive;
    return res.status(201).render('notification', {data});
};
authController.checkLogIn = async (req, res, next) => {
    if (req.session.userId) {
        next();
    } else {
        res.redirect('/sign-in')
    }
}
authController.logIn = async (req, res) => {
    let { username, password } = req.body;
    let userInfo = {};
    userInfo.username = username;
    let logIn = await userService.logIn(userInfo);
    console.log(logIn);
    if (logIn != null && logIn.local.isActive == true  ) {
        await bcrypt.compare(password, logIn.local.password, async (err, result) => {
                try {
                    if (result) {
                        req.session.userId = logIn._id;
                        res.redirect('/post/home')
                    }   else {
                        let error = {msg:'Sai mật khẩu'};
                        res.render('sign-in',  {error})
                    }
                } catch (error) {
                    res.status(500).send(error)
                }
            }
        );
    } else if (logIn != null && logIn.local.isActive == false) {
        let error = {msg:'Tài khoản chưa được kích hoạt hãy liên hệ quản trị viên hoặc check lại email đăng ký của bạn'};
        res.render('sign-in',  {error})
    } else {
        let error = {msg:'Tài khoản không tồn tại'};
        res.render('sign-in',  {error}) 
    }
}
authController.logOut = (req, res) => {
    res.clearCookie('express.sid');
    return res.redirect('back');
}
module.exports = authController;
