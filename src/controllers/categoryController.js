const categoryService = require('../services/categoryService');

const createCategoryController =  async (req, res) => {
    try {
        let idPost = req.params;
        let postInfo = req.body
        let edit = await editPost(idPost, postInfo)
        if (edit) {
            return res.json({
                error: false,
                status: 200,
                message: 'Cập nhật bài viết thành công',
                data: edit
            })
        };
        {
            return res.json({
                error: true,
                status: 500,
                message: 'Không tìm thấy bài viết',
            })
        }
    } catch (err) {
        return res.json({
            error: true,
            status: 500,
            message: console.log(err),
        })
    }
}
