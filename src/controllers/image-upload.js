
const upload = require('../services/image-upload');
const categoryService = require('../services/categoryService')


const singleUpload = upload.single('upload');
const featuredImageUpload = upload.single('featuredImage');

const checkUpload = async  (req, res, next) => {
    const file = req.file;
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }
      res.json({
        url: file.location
      })
}


module.exports = {
    checkUpload,
    singleUpload,
    featuredImageUpload
}