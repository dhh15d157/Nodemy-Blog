const postService = require("../services/postService");
const userService = require('../services/userService')
const categoryService = require("../services/categoryService");
const commentService = require('../services/commentService');
const imageUploadController = require("../services/image-upload");
const { createTransport } = require("nodemailer");
const { createPost, editPost, findPost } = require("../services/postService");
const { featuredImageUpload } = require("./image-upload");
const moment = require('moment');
const validate = require('../utils/validation');
var mongoose = require('mongoose');




const deletePostController = async (req, res) => {
  try {
    let idPost = req.params.idPost;
    let deletePost = await postService.deletePost(idPost);
    if (deletePost) {
      return res.json({
        error: false,
        status: 200,
        message: "Xoá bài viết thành công",
      });
    }
    {
      return res.json({
        error: true,
        status: 500,
        message: "Không tìm thấy bài viết",
      });
    }
  } catch (err) {
    return res.json({
      error: true,
      status: 500,
      message: console.log(err),
    });
  }
};
const editPostController = async (req, res) => {
  try {
    let idPost = req.params;
    let postInfo = req.body;
    let edit = await editPost(idPost, postInfo);
    if (edit) {
      return res.json({
        error: false,
        status: 200,
        message: "Cập nhật bài viết thành công",
        data: edit,
      });
    }
    {
      return res.json({
        error: true,
        status: 500,
        message: "Không tìm thấy bài viết",
      });
    }
  } catch (err) {
    return res.json({
      error: true,
      status: 500,
      message: console.log(err),
    });
  }
};
const createPostController = async (req, res) => {
  try {
    let postInfo = req.body;
    let featuredImage = req.file.location;
    postInfo.featuredImage = featuredImage;
    postInfo.author = req.session.userId;
    let createIt = await createPost(postInfo);
    console.log(createIt);
    return res.redirect("http://localhost:3000/post/home");
  } catch (err) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(err),
    });
  }
};
const findPostController = async (req, res) => {
  try {
    let idPost = req.params;
    function formatDate (param) {
      let data = moment(param).format('MMM Do YYYY');
      return data;
    }
    let getCategory = await categoryService.getCategory();
    let getSinglePost = await postService.findPost(idPost.idPost);
    console.log(getSinglePost);
    let getUser = await userService.getUser(req.session.userId);
    return res.render('singlePost', {getSinglePost, formatDate, getUser, getCategory })
  } catch (error) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(error),
    });
  }
};
const loadNewPost = async (req, res) => {
  try {
    let getAllData = {};
    let getCategory = await categoryService.getCategory(getAllData);
    if (getCategory) {
      res.render("createPost", { getCategory });
    }
  } catch (error) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(error),
    });
  }
};
const loadHomePageController = async (req, res, next) => {
  try {
    let getUser = await userService.getUser(req.session.userId);
    let getCategory = await categoryService.getCategory();
    // function format date:
    function formatDate (param) {
      let data = moment(param).format('MMM Do YYYY');
      return data;
    }
     // pagination
    let PAGE_SIZE = 4;
    let pageNumber = parseInt(req.query.page) ;
    let rs = await postService.getAllPost(PAGE_SIZE, pageNumber);
    let { count, getPost } = rs;
    let totalNav = Math.ceil(count/PAGE_SIZE); 
    //
    return res.render("home", { getCategory, totalNav, getPost, formatDate, getUser });

  } catch (err) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(err),
    });
  }
};
////////////////////
const loadContentPost = async (req, res) => {
  try {
    let idPost = req.params;
    let getSinglePost = await postService.findPost(idPost.idPost);
    if (getSinglePost) {
      return res.send({
        data: getSinglePost
      })
    }
    {
      return res.status(200).json({
        error: false,
        status: 200,
        message: "Không tìm thấy bài viết",
      });
    }
  } catch (error) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(error),
    });

  }
}
const addCommentPost = async (req, res) => {
  try {
      // create cmt in db
    let {owner, content} = req.body;
    let commentInfo = {owner, content};
    let addComment = await commentService.addComment(commentInfo);
    // add id cmt in field cmt of post
    let idComment = addComment._id.toString(); 
    let idPost = req.body.idPost.toString();
    let updatePost = await postService.addComment(idPost, idComment);
    res.redirect('back');

  } catch (err) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(err)
  }) 
  }
};
const addReplyCommentPost = async (req, res) => {
  try {
      // create  reply cmt in db
      console.log(req.body);
    let {owner, content} = req.body;
    let replyCommentInfo = {owner, content};
    let addReplyComment = await commentService.addReplyComment(replyCommentInfo);
    // add id reply cmt in field reply cmt of comments
    let idReplyComment = addReplyComment._id.toString(); 
    let idComment = req.body.idComment;

    console.log('đây là idreply:', idReplyComment);
    console.log('đây là idcmt:',idComment);

    let updateComment = await commentService.addId(idComment, idReplyComment);
    res.redirect('back');

  } catch (err) {
    return res.status(500).json({
      error: true,
      status: 500,
      message: console.log(err)
  }) 
  }
}

const postController = {
  deletePostController,
  editPostController,
  createPostController,
  findPostController,
  loadNewPost,
  loadHomePageController,
  loadContentPost,
  addCommentPost,
  addReplyCommentPost
};
module.exports = postController;
