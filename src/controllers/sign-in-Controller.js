
const signInController = async (req, res) => {

    try {
        console.log(req.session.userId);
        if (req.session.userId) {
            return res.redirect('/post/home');
        } 
        let error;
        res.render('sign-in', {error});
 
    } catch (error) {
        console.log(error);
    }
}
module.exports = signInController;