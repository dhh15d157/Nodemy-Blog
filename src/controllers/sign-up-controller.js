



const signUpController = async (req, res) => {
    try {
        console.log(req.session.userId);
        if (req.session.userId) {
            return res.redirect('/post/home');
        }
        let error;
        return res.render('sign-up', { error });
 
    } catch (error) {
        console.log(error);
    }
}
module.exports = signUpController;