const viValidationErrors = {
    emailIncorrect: 'Email phải có dạng example@domain.com!',
    emailExist: 'Email đăng ký đã tồn tại',
    passwordIncorrect:
        'Mật khẩu phải chứa ít nhất 8 ký tự bao gồm chữ Hoa, chữ thường, chữ số và ký tự đặc biệt!',
    passwordConfirmationIncorrect: 'Mật khẩu nhập lại không chính xác!',
    userActiveError:
        'Tài khoản đã được đăng ký nhưng chưa kích hoạt. Vui lòng kiểm tra email để kích hoạt tài khoản',
    userDelete:
        'Tài khoản này đã bị gỡ bỏ khỏi hệ thống. Nếu có thắc mắc, vui lòng liên hệ với quản trị viên để được hỗ trợ',
    sendMailError:
        'Có lỗi trong quá trình gửi email. Vui lòng liên hệ với quản trị viên để được hỗ trợ',
    tokenUndefined:
        'Tài khoản đã được kích hoạt. Vui lòng đăng nhập vào blog để sử dụng',
};

const viValidationSuccess = {
    userCreateSuccess: (name) => {
        return `Đăng ký tài khoản ${name} thành công! Vui lòng kiểm tra email để xác nhận tài khoản.`;
    },
    accountActive:
        'Tài khoản kích hoạt thành công. Bạn có thể đăng nhập vào blog để sử dụng!',
};

const viAuth = {
    subjectEmail: 'Blog Nodemy - Xác nhận đăng ký tài khoản',
    templateEmail: (linkVerify, username) => {
        return `
            <h2>Hi ${username}</h2>
            <h3>Bạn vừa đăng ký tài khoản mới tại Blog Nodemy.</h3>
            <h3>Vui lòng nhấn vào liên kết bên dưới để xác nhận tài khoản:</h3>
            <h4><a href="${linkVerify}">${linkVerify}</a></h4>
            <h3>Nếu không phải bạn là người đăng ký. Vui lòng bỏ qua email này</h3>
            <h4>Trân trọng cảm ơn</h4>
        `;
    },
};

module.exports = {
    viValidationErrors,
    viValidationSuccess,
    viAuth,
};
