const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const categorySchema = new Schema(
  {
    slug_name: String,
    name: String
  }
);
module.exports = mongoose.model("categories", categorySchema);
