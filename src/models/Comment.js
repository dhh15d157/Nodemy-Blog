const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const commentSchema = new Schema(
  {
    owner: {
      type: String,
      ref: "users",
    },
    content: String,
    replyComment: [{
      type: String,
      ref: 'replycomments'
    }]
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("comments", commentSchema);
