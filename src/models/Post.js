const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const PostSchema = new Schema(
  {
    author: {
      type: String,
      ref: "users",
      default: 'Hoàng Hải'
    },
    content: String,
    comments: [{
      type: String,
      ref: "comments",
    }],
    category: {
      type: String,
      ref: "categories",
    },
    title: String,
    featuredImage: String
  },
  {
    timestamps: true,
  }
);


module.exports = mongoose.model("posts", PostSchema);
