const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema(
    {
        username: {
            type: String,
        },
        avatar: {
            type: String,
            default: 'https://image-nodemyblog.s3-ap-southeast-1.amazonaws.com/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg',
        },
        role: {
            type: String,
            default: 'user',
        },
        local: {
            email: {
                unique: true,
                type: String,
            },
            password: {
                type: String,
            },
            isActive: {
                type: Boolean,
                default: false,
            },
            verifyToken: {
                type: String,
            },
        },
        facebook: {
            uid: String,
            token: String,
            email: String,
        },
        google: {
            uid: String,
            token: String,
            email: String,
        },
        deletedAt: {
            type: Number,
            default: null,
        },
    },
    {
        collection: 'users',
        timestamps: true,
    }
);
module.exports = mongoose.model('users', UserSchema);


