const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const replyCommentSchema = new Schema(
  {
    owner: {
      type: String,
      ref: "users",
    },
    content: String
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("replycomments", replyCommentSchema);