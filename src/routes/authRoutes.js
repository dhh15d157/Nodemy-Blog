const router = require('express-promise-router')();
const authController = require('../controllers/authController');
const validation = require('../utils/validation');
const authRoutes = require('express').Router();


authRoutes.post('/register',validation.registerErrors, authController.Register );
authRoutes.get('/verify/:token', authController.getVerifyAccount);
authRoutes.post('/sign-in', authController.logIn);
authRoutes.get('/log-out', authController.logOut);

/* router
    .route('/register')
    .post(validation.registerErrors, authController.Register);

router
    .route('/verify/:token')
    .get(authController.getVerifyAccount);
 */
module.exports = authRoutes;
