const userRoutes = require('./userRoutes');
const authRoutes = require('./authRoutes');
const postRoutes = require('./postRoutes');
const uploadRoutes = require('./image-upload');
const signInRoutes = require('./sign-in-Routes');
const signUpRoutes = require('./sign-up-Routes');

const routes = (app) => {
    app.use('/user', userRoutes);
    app.use('/auth', authRoutes);
    app.use('/post', postRoutes);
    app.use('/upload', uploadRoutes);
    app.use('/sign-in', signInRoutes);
    app.use('/sign-up', signUpRoutes);
};

module.exports = routes;
