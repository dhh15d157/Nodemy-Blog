const postController = require('../controllers/postController');
const authController = require('../controllers/authController');
const upload = require('../controllers/image-upload')
const { getPostController } = require('../controllers/postController');
const postRouter = require('express').Router();

postRouter.delete('/:idPost', postController.deletePostController );
postRouter.put('/:idPost',authController.checkLogIn, postController.editPostController);
postRouter.post('/', upload.featuredImageUpload, postController.createPostController);
postRouter.get('/home', postController.loadHomePageController);
postRouter.get('/new', authController.checkLogIn, postController.loadNewPost);
postRouter.get('/content/:idPost', postController.loadContentPost); // call ajax show content in single post
postRouter.get('/:idPost', postController.findPostController);
postRouter.post('/addComment', postController.addCommentPost); // call ajax add commnent
postRouter.post('/addReplyComment', postController.addReplyCommentPost); // call ajax add  reply commnent






module.exports = postRouter;
