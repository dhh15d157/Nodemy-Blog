
const signIn = require('../controllers/sign-in-Controller');
const signInRoute = require('express').Router();

signInRoute.get('/', signIn);
module.exports = signInRoute;