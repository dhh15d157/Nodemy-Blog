
const signUp = require('../controllers/sign-Up-Controller');
const signUpRoute = require('express').Router();

signUpRoute.get('/', signUp);
module.exports = signUpRoute;