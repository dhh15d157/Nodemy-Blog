const router = require('express').Router();
const authController = require('../controllers/authController');
const validation = require('../utils/validation');

router.post('/', authController.logIn)

module.exports = router;
