const userModel = require('../models/User');
const sendMail = require('../configs/mailer');
const { viAuth } = require('../lang/vi');
const { v4: uuidv4 } = require('uuid');

const authService = {};

authService.userRegister = async (email, password, protocol, host) => {
    // get info send back to DB
    let userRegister = {
        username: email.split('@')[0],
        local: {
            password,
            email,
            verifyToken: uuidv4(),
        },
    };

    let user = new userModel(userRegister);
    await user.save();

    let linkVerify = `${protocol}://${host}/auth/verify/${userRegister.local.verifyToken}`;
    // send confirmation email to user
    return await sendMail(
        email,
        viAuth.subjectEmail,
        viAuth.templateEmail(linkVerify, userRegister.username)
    );
};
// check token of user
authService.getVerifyAcount = async (token) => {
    return await userModel.findOneAndUpdate(
        {
            'local.verifyToken': token,
        },
        {
            'local.isActive': true,
            'local.verifyToken': null,
        }
    );
};

module.exports = authService;
