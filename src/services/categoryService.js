const categoryModel = require('../models/Category');



const createCategory = (categoryInfo) => {
    let create = categoryModel.create(categoryInfo);
    return create
}
const getCategory = (categoryInfo) => {
    let find = categoryModel.find(categoryInfo)
    return find
}

module.exports = {
    createCategory,
    getCategory
}