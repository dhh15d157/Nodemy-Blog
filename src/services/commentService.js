const commentModel = require('../models/Comment');
const replyCommentModel = require('../models/replyComment');

let findComment = async (id) => {
    let find =  await commentModel.findById(id).populate({
        path: 'owner',
    });
    return find;
}
let findReplyComment = async (id) => {
    return await replyCommentModel.findById(id).populate('owner');
}
let addReplyComment = async (info) => {
    return await replyCommentModel.create(info);
}
let addComment = async (info) => {
    return await commentModel.create(info)
}
let addId = async (id, content) => {
    let addIt = await commentModel.findOneAndUpdate({
        _id: id
      } ,{
        $push: {replyComment: content}
      },{
        new: true
      });
      return addIt;
}


module.exports = {
    findComment,
    findReplyComment,
    addReplyComment,
    addComment,
    addId
}