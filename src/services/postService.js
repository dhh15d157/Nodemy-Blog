const postModel = require("../models/Post");
const commentModel = require('../models/Comment')
const userModel = require('../models/User')
const replyComment = require('../models/replyComment')
const createPost = (postInfo) => {
  let create = postModel.create(postInfo);
  return create;
};
const editPost = async (id, postInfo) => {
  let edit = await postModel.findOneAndUpdate(
    {
      _id: id.idPost,
    },
    postInfo,
    {
      new: true,
    }
  );
  return edit;
};
const deletePost = async (id) => {
  let deleteIt = await postModel.deleteOne({ _id: id });
  return deleteIt;
};
const getAllPost = async (PAGE_SIZE, pageNumber) => {
  let count = await postModel.countDocuments();
  let getPost = await postModel.find({}).limit(PAGE_SIZE).skip(PAGE_SIZE*pageNumber - PAGE_SIZE);
  return { count, getPost };
};
const addComment = async (id, content) => {
  let addIt = await postModel.findOneAndUpdate({
    _id: id
  } ,{
    $push: {comments: content}
  },{
    new: true
  });
  return addIt;
};
const findPost = async (id) => {
  let findIt = await postModel.findById({ _id: id }).populate({
    path: 'comments',
    populate: {
      path: 'owner'
    }
  })
  .populate({
    path: 'comments',
    populate: {
      path: 'replyComment',
      populate: {
        path: 'owner'
      }
    }
  })
  .populate('author');
  return findIt;
};


module.exports = {
  createPost,
  editPost,
  deletePost,
  findPost,
  getAllPost,
  addComment
};
