const userModel = require('../models/User');
const { v4: uuidv4 } = require('uuid');

const userService = {};

userService.findByEmail = async (email) => {
    return await userModel.findOne({ 'local.email': email });
};

userService.findByToken = async (token) => {
    return await userModel.findOne({
        'local.verifyToken': token,
    });
};

userService.deleteUserByEmail = async (email) => {
    return await userModel.findOneAndDelete({ 'local.email': email });
};
userService.logIn = async (userInfo) => {
    return await userModel.findOne(userInfo);
}
userService.getUser = async (id) => {
    return await userModel.findById(id);
}

module.exports = userService;
