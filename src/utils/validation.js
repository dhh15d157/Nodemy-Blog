const { check } = require('express-validator');
const { viValidationErrors } = require('../lang/vi');

let registerErrors = [
    check('email', viValidationErrors.emailIncorrect).isEmail().trim(),
    check('password', viValidationErrors.passwordIncorrect).matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/
    ),
    check(
        'passwordConfirmation',
        viValidationErrors.passwordConfirmationIncorrect
    ).custom((value, { req }) => {
        return value === req.body.password;
    }),
];

module.exports = {
    registerErrors
};
